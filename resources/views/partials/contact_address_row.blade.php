<div class="row  m-2">
    <input name="address_ids[]" type="hidden" value="{{isset($address) ? $address->id : ''}}">
    <div class="form-group col-md-4">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email[]" value="{{isset($address) ? $address->email : ''}}" placeholder="Enter email">
        @if($errors->has('email.*'))
            <div class="error m-t-10 text-danger">{{ $errors->first('email.*') }}</div>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label for="contact_number">Contact Number</label>
        <input type="text" class="form-control" name="contact_number[]" value="{{isset($address) ? $address->contact_number : ''}}" placeholder="Enter Contact Number">
        @if($errors->has('contact_number.*'))
            <div class="error m-t-10 text-danger">{{ $errors->first('contact_number.*') }}</div>
        @endif
    </div>
    <hr>
</div>
