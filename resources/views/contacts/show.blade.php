@extends('layouts.app')

@section('title', 'Show Contact')

@section('content')
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Contact Details for {{ $contact->first_name . ' ' . $contact->last_name }}</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('contacts.index')}}">Contacts</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Details</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Contact Number </th>
                                        <th>Email</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contact->addresses as $address)
                                        <tr>
                                            <td>{{ $address->contact_number }}</td>
                                            <td>{{ $address->email }}</td>
                                            <td>
                                                <a class="btn btn-outline-danger" href="{{route('contacts.address.delete', encodeId($address->id))}}"><i class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection