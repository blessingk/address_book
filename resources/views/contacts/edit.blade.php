@extends('layouts.app')

@section('title', 'Update Contact')

@section('content')
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Update Contact</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('contacts.index')}}">Contacts</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Update Contact</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <form class="form-horizontal" action="{{route('contacts.update', encodeId($contact->id))}}" method="POST">
                            {{csrf_field()}}
                            <div class="card-body">
                                <h4 class="card-title">Update Contact details</h4>
                                <div class="form-group row">
                                    <label for="first_name" class="col-sm-3 text-left control-label col-form-label">Firstname</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="{{$contact->first_name}}" name="first_name" id="first_name" placeholder="First Name Here">
                                        @if($errors->has('first_name'))
                                            <div class="error m-t-10 text-danger">{{ $errors->first('first_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="last_name" class="col-sm-3 text-left control-label col-form-label">Lastname</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="{{$contact->last_name}}" id="last_name" name="last_name" placeholder="Surname Here">
                                        @if($errors->has('last_name'))
                                            <div class="error m-t-10 text-danger">{{ $errors->first('last_name') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <hr>

                                <h3>Contact Numbers and Emails</h3>
                                <div class="address-row">
                                    @foreach ($contact->addresses as $address)
                                        @include('partials.contact_address_row')
                                    @endforeach
                                </div>
                                <a href="#" class="add-address btn btn-sm btn-outline-dark ml-lg-5"><i class="fas fa-plus-circle fa-2x"></i></a><br>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection