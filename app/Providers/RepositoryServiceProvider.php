<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contact\ContactRepository;
use App\Repositories\Contact\ContactRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ContactRepositoryInterface::class , ContactRepository::class);
    }
}
