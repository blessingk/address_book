<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert as swal;
use App\Repositories\Contact\ContactRepositoryInterface;

class ContactController extends Controller
{
    /**
     * @var ContactRepositoryInterface
     */
    private $contactRepository;

    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function index()
    {
        $contacts = $this->contactRepository->getAllContacts();
        return view('contacts.index', compact('contacts'));
    }

    public function create()
    {
        return view('contacts.create');
    }

    public function show($encodedContactId)
    {
        $contact = $this->contactRepository->getContactById($encodedContactId);
        return view('contacts.show', compact('contact'));
    }

    public function edit($encodedContactId)
    {
        $contact = $this->contactRepository->getContactById($encodedContactId);
        return view('contacts.edit', compact('contact'));
    }

    public function update(Request $request, $encodedContactId)
    {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email.*' => 'email|max:255'
        ]);

        $this->contactRepository->updateContact($request, $encodedContactId);
        swal::success('Contact updated successfully.', '')->toToast();
        return redirect()->route('contacts.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email.*' => 'email|max:255'
        ]);

        $this->contactRepository->storeContact($request);
        swal::success('Contact created successfully.', '')->toToast();
        return redirect()->route('contacts.index');
    }


    public function delete($encodedContactId) {
        $this->contactRepository->deleteContact($encodedContactId);
        swal::success('Contact deleted successfully.', '')->toToast();
        return redirect()->route('contacts.index');
    }

    public function addContactAddressRow() {
        $html = \View::make('partials.contact_address_row')->render();
        return \Response::json(array(
            'html' => $html
        ));
    }

    public function deleteContactAddress($encodedContactAddressId)
    {
        $this->contactRepository->deleteContactAddress($encodedContactAddressId);
        swal::success('Contact detail deleted successfully.', '')->toToast();
        return redirect()->route('contacts.index');
    }

}
