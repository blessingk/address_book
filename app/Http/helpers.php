<?php

function decodeId($encodedId) : int {
    $decodedData = Hashids::decode($encodedId);
    $id = array_shift($decodedData);
    if (!$id && $id !== 0) throw new \Exception("Invalid Encrypted ID [$encodedId]");
    return intval($id);
}

function encodeId($id) : string {
    return Hashids::encode($id);
}