<?php

namespace App\Repositories\Contact;

use DB;
use App\Models\Contact;
use App\Models\ContactAddress;
use Illuminate\Http\Request;

class ContactRepository implements ContactRepositoryInterface
{

    /**
     * @var Contact
     */
    private $contact;
    /**
     * @var ContactAddress
     */
    private $contactAddress;

    public function __construct(Contact $contact, ContactAddress $contactAddress)
    {
        $this->contact = $contact;
        $this->contactAddress = $contactAddress;
    }

    public function getAllContacts()
    {
        return $this->contact::all();
    }

    public function getContactById($encodedContactId)
    {
        $decodedContactId = decodeId($encodedContactId);
        return $this->contact::findOrFail($decodedContactId);
    }

    public function updateContact(Request $request, $encodedContactId)
    {
        DB::beginTransaction();

        try {
            $decodedContactId = decodeId($encodedContactId);
            $contact = $this->contact->findOrFail($decodedContactId);
            $contact->first_name = $request->first_name;
            $contact->last_name = $request->last_name;
            $contact->save();

            $this->createOrUpdateContactAddress($request, $contact);

            DB::commit();
            return $contact;

        } catch (\Exception $exception) {
            DB::rollback();
            return $exception->getMessage();
        }
    }

    public function storeContact(Request $request)
    {
        DB::beginTransaction();

        try {
            $contact = new $this->contact;
            $contact->first_name = $request->first_name;
            $contact->last_name = $request->last_name;
            $contact->save();

            $this->createOrUpdateContactAddress($request, $contact);

            DB::commit();

            return $contact;

        } catch (\Exception $exception) {
            DB::rollback();
            return $exception->getMessage();
        }
    }

    public function deleteContact($encodedContactId)
    {
        $decodedContactId = decodeId($encodedContactId);
        $this->contactAddress::where('contact_id', $decodedContactId)->delete();
        $this->contact::where('id', $decodedContactId)->delete();
    }

    public function createOrUpdateContactAddress(Request $request, $contact)
    {
        foreach ($request->address_ids as $key => $id) {
            if(isset($id)) {
                $contactAddress = $this->contactAddress::findOrFail($id);
            } else {
                $contactAddress = new $this->contactAddress;
            }
            $contactAddress->contact_id = $contact->id;
            $contactAddress->email = $request->email[$key];
            $contactAddress->contact_number = $request->contact_number[$key];
            $contactAddress->save();
        }
    }

    public function deleteContactAddress($encodedContactAddressId)
    {
        $decodedContactAddressId = decodeId($encodedContactAddressId);
        $this->contactAddress::where('id', $decodedContactAddressId)->delete();
    }
}