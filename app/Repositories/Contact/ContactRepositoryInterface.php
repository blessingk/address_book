<?php

namespace App\Repositories\Contact;

use Illuminate\Http\Request;

interface ContactRepositoryInterface
{
    public function getAllContacts();

    public function getContactById($encodedContactId);

    public function updateContact(Request $request, $encodedContactId);

    public function storeContact(Request $request);

    public function deleteContact($encodedContactId);

    public function deleteContactAddress($encodedContactAddressId);
}