<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function addresses()
    {
        return $this->hasMany('App\Models\ContactAddress');
    }
}
