<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactAddress extends Model
{
    protected $fillable = ['email', 'contact_number', 'contact_id'];

    public function contact()
    {
        return $this->belongsTo('App\Models\Contact');
    }
}
