$(document).ready(function() {

    $(document).on('click', '.add-address', function () {
        $.ajax({
            url: "/contacts/add/address/row",
            type: 'GET',
            data: {},
            success: function(response) {
                $('.address-row').append(response.html);
            },
            error: function(data) {}
        })
    });
});