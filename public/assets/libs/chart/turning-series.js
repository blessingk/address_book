$(function() {
    var datasets = {
        "south africa": {
            label: "SOUTH AFRICA",
            data: [
                [2015, 494],
                [2016, 970],
                [2017, 348],
                [2018, 149],
                [2019, 305],
                [2020, 345],
            ]
        },
        "rwanda": {
            label: "RWANDA",
            data: [
                [2015, 194],
                [2016, 920],
                [2017, 448],
                [2018, 959],
                [2019, 195],
                [2020, 325],
            ]
        },
        "manchester": {
            label: "MANCHESTER",
            data: [
                [2015, 294],
                [2016, 560],
                [2017, 448],
                [2018, 349],
                [2019, 705],
                [2020, 575],
            ]
        },
        "lebanon": {
            label: "LEBANON",
            data: [
                [2015, 374],
                [2016, 160],
                [2017, 248],
                [2018, 449],
                [2019, 765],
                [2020, 175],
            ]
        },
        "malawi": {
            label: "MALAWI",
            data: [
                [2015, 304],
                [2016, 660],
                [2017, 798],
                [2018, 909],
                [2019, 205],
                [2020, 375],
            ]
        },
    };
    var colors = [
        "#005CDE",
        "#ffb848",
        "#00A36A",
        "#7D0096",
        "#992B00"
    ];
    // hard-code color indices to prevent them from shifting as
    // countries are turned on/off
    var i = 0;
    $.each(datasets, function(key, val) {
        val.color = colors[i];
        ++i;
    });

    // insert checkboxes 
    var choiceContainer = $("#choices");
    $.each(datasets, function(key, val) {
        choiceContainer.append('<input type="checkbox" name="' + key +
            '" checked="checked" id="id' + key + '">' +
            '<label for="id' + key + '">' +
            val.label + '</label>');
    });
    choiceContainer.find("input").click(plotAccordingToChoices);


    function plotAccordingToChoices() {
        var data = [];

        choiceContainer.find("input:checked").each(function() {
            var key = $(this).attr("name");
            if (key && datasets[key])
                data.push(datasets[key]);
        });

        if (data.length > 0)
            $.plot($("#placeholder"), data, {
                yaxis: { min: 0 },
                xaxis: { tickDecimals: 0 }
            });
    }

    plotAccordingToChoices();
});