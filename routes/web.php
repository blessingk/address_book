<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => '/contacts'], function () {
    Route::get('/', 'ContactController@index')->name('index');
    Route::get('/index', 'ContactController@index')->name('contacts.index');
    Route::get('/create', 'ContactController@create')->name('contacts.create');
    Route::post('/{id}/update', 'ContactController@update')->name('contacts.update');
    Route::get('/{id}/delete', 'ContactController@delete')->name('contacts.delete');
    Route::get('/{id}/edit', 'ContactController@edit')->name('contacts.edit');
    Route::post('/store', 'ContactController@store')->name('contacts.store');
    Route::get('/{id}/show', 'ContactController@show')->name('contacts.show');
    Route::get('/address/{id}/delete', 'ContactController@deleteContactAddress')->name('contacts.address.delete');
    Route::get('/add/address/row', 'ContactController@addContactAddressRow');
});
